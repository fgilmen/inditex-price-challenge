package com.indx.codechallenge.application.rest;

import com.indx.codechallenge.domain.price.GetPriceBO;
import com.indx.codechallenge.domain.price.PriceService;
import com.indx.codechallenge.domain.price.utils.LocalDateTimeConversionUtils;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/price")
@AllArgsConstructor
public class PriceController {

    private final ConversionService conversionService;
    private final LocalDateTimeConversionUtils localDateTimeConversionUtils;
    private final PriceService priceService;

    @GetMapping(produces = "application/json")
    public ResponseEntity<GetPriceResponse>
        getPrice(@RequestParam(name = "applicationDate")  String applicationDate,
                 @RequestParam(name = "productId") String productId,
                 @RequestParam(name = "brandId") @Min(value = 1, message = "No negative") @Max(value = 999999) Long brandId
    ) throws Throwable {

        return ResponseEntity.ok().body(
                conversionService.convert(priceService.getPrice(getGetPriceBO(applicationDate, productId, brandId))
                        , GetPriceResponse.class)
        );
    }

    private GetPriceBO getGetPriceBO(final String applicationDate, final String productId, final Long brandId) {
        return GetPriceBO.builder()
                .brandId(brandId)
                .productId(productId)
                .ldtApplicationDate(localDateTimeConversionUtils.convertStringToLocalDateTime(applicationDate))
                .build();
    }
}
