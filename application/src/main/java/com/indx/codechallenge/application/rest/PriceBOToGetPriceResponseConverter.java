package com.indx.codechallenge.application.rest;

import com.indx.codechallenge.domain.price.PriceBO;
import com.indx.codechallenge.domain.price.utils.LocalDateTimeConversionUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PriceBOToGetPriceResponseConverter implements Converter<PriceBO, GetPriceResponse> {

    private final LocalDateTimeConversionUtils localDateTimeConversionUtils;

    public PriceBOToGetPriceResponseConverter(final LocalDateTimeConversionUtils localDateTimeConversionUtils) {
        this.localDateTimeConversionUtils = localDateTimeConversionUtils;
    }

    @Override
    public GetPriceResponse convert(final PriceBO priceBO) {

        return GetPriceResponse.builder()
                .brandId(priceBO.getBrandId())
                .finalPrice(priceBO.getFinalPrice())
                .endDate(localDateTimeConversionUtils.convertLocalDateTimeToString(priceBO.getEndDate()))
                .startDate(localDateTimeConversionUtils.convertLocalDateTimeToString(priceBO.getStartDate()))
                .isoCurrency(priceBO.getIsoCurrency())
                .priceList(priceBO.getPriceList())
                .productId(priceBO.getProductId())
                .build();
    }
}
