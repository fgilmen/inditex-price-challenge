package com.indx.codechallenge.application.rest;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
@EqualsAndHashCode
@ToString
@Getter
@Builder
public class GetPriceResponse {
    private String productId;
    private Long brandId;
    private Integer priceList;
    private String startDate;
    private String endDate;
    private Float finalPrice;
    private String isoCurrency;
}
