package com.indx.codechallenge.application.rest.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseResponse {
    private StatusResponse callStatus;

    public BaseResponse() {
        this.callStatus = new StatusResponse();
    }
}
