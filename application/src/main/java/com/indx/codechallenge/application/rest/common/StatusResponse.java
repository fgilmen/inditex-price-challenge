package com.indx.codechallenge.application.rest.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusResponse {
    private String responseStatus;
    private String message;

    public StatusResponse() {
        this.message = "";
        this.responseStatus = "OK";
    }
}
