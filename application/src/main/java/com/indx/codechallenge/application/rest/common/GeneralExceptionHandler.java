package com.indx.codechallenge.application.rest.common;

import com.indx.codechallenge.domain.price.ElementsNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.time.format.DateTimeParseException;

@ControllerAdvice
public class GeneralExceptionHandler {

    public static final String RESPONSE_STATUS_ERROR = "ERROR";

    @ExceptionHandler(ElementsNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public BaseResponse handlePriceNotFound(final ElementsNotFoundException exception){

        BaseResponse response = new BaseResponse();
        response.getCallStatus().setResponseStatus(RESPONSE_STATUS_ERROR);
        response.getCallStatus().setMessage("Price not found");

        return response;

    }
    @ExceptionHandler(DateTimeParseException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public BaseResponse handleDateTimeParseException(final DateTimeParseException exception){

        BaseResponse response = new BaseResponse();
        response.getCallStatus().setResponseStatus(RESPONSE_STATUS_ERROR);
        response.getCallStatus().setMessage("Bad time parse exception");

        return response;

    }
    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public BaseResponse handleMissingParameterException(final MissingServletRequestParameterException exception){

        BaseResponse response = new BaseResponse();
        response.getCallStatus().setResponseStatus(RESPONSE_STATUS_ERROR);
        response.getCallStatus().setMessage(exception.getParameterName() + " not present");

        return response;

    }
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public BaseResponse handleArgumentTypeMismatchException(final MethodArgumentTypeMismatchException exception){

        BaseResponse response = new BaseResponse();
        response.getCallStatus().setResponseStatus(RESPONSE_STATUS_ERROR);
        response.getCallStatus().setMessage(exception.getName() + " is type " + exception.getParameter().getParameterType().toString());

        return response;

    }
    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public BaseResponse handleRestException(final Exception exception){

        BaseResponse response = new BaseResponse();
        response.getCallStatus().setResponseStatus(RESPONSE_STATUS_ERROR);
        response.getCallStatus().setMessage("Internal server error");

        return response;

    }

}
