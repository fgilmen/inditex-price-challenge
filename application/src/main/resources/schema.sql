drop table if EXISTS BRANDS;
drop table if EXISTS PRICES;
drop table if EXISTS PRODUCTS;

create table BRANDS (
    ID bigint not null,
    NAME varchar(255) not null,
    primary key (ID)
);
create table PRICES (
    PRICE float(24) not null,
    PRICE_LIST integer not null,
    PRIORITY integer not null,
    BRAND_ID bigint,
    END_DATE timestamp,
    ID bigint generated by default as identity,
    START_DATE timestamp not null,
    CURR varchar(255),
    PRODUCT_ID varchar(10),
    primary key (ID)
);
create table PRODUCTS (
    ID varchar(255) not null,
    NAME varchar(255) not null,
    primary key (ID)
);
