package codechallenge.app.integration.domain;

import com.indx.codechallenge.application.Application;
import com.indx.codechallenge.domain.price.ElementsNotFoundException;
import com.indx.codechallenge.domain.price.GetPriceBO;
import com.indx.codechallenge.domain.price.PriceBO;
import com.indx.codechallenge.domain.price.PriceService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest(
        classes = Application.class)
public class PriceServiceTest {

    private static final String PRODUCT_ID = "35455";
    private static final long BRAND_ID = 1L;
    
    @Autowired
    PriceService priceService;

    @Value("${codechallenge.pattern.stringtolocaldatetime}")
    private String dateTimeFormatter;

    @Test
    @DisplayName("request at 10:00 a.m. on the 14th for product 35455 for brand 1")
    public void test1() throws Throwable {
        var result = getPrices("2020-06-14-10:00:00");

        assertEquals(Float.valueOf(35.5f), result.getFinalPrice());
    }

    @Test
    @DisplayName("request at 4:00 p.m. on the 14th for product 35455 for brand 1")
    public void test2() throws Throwable {
        var result = getPrices("2020-06-14-16:00:00");

        assertEquals(Float.valueOf(25.45f), result.getFinalPrice());
    }

    @Test
    @DisplayName("request at 9:00 p.m. on the 14th for product 35455 for brand 1")
    public void test3() throws Throwable {
        var result = getPrices("2020-06-14-21:00:00");

        assertEquals(Float.valueOf(35.5f), result.getFinalPrice());
    }

    @Test
    @DisplayName("request at 10:00 a.m. on the 15th for product 35455 for brand 1")
    public void test4() throws Throwable {
        var result = getPrices("2020-06-15-10:00:00");

        assertEquals(Float.valueOf(30.5f), result.getFinalPrice());
    }

    @Test
    @DisplayName("request at 9:00 p.m. on the 16th for product 35455 for brand 1")
    public void test5() throws Throwable {
        var result = getPrices("2020-06-16-21:00:00");

        assertEquals(Float.valueOf(38.95f), result.getFinalPrice());
    }

    @Test
    @DisplayName("exception thrown in no data case")
    public void test6(){
        assertThrows(ElementsNotFoundException.class, () -> getPrices("2021-06-16-21:00:00"));
    }

    private PriceBO getPrices(String date) throws Throwable {

        return priceService.getPrice(GetPriceBO.builder()
                        .brandId(BRAND_ID)
                        .ldtApplicationDate(LocalDateTime.parse(date, DateTimeFormatter.ofPattern(dateTimeFormatter)))
                        .productId(PRODUCT_ID)
                .build());
    }
}
