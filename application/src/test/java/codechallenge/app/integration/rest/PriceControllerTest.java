package codechallenge.app.integration.rest;

import com.indx.codechallenge.application.Application;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        classes = Application.class)
@AutoConfigureMockMvc
public class PriceControllerTest {
    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("request at 10:00 a.m. on the 14th for product 35455 for brand 1")
    public void test1() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders
                        .get("/price")
                        .queryParam("applicationDate", "2020-06-14-10:00:00")
                        .queryParam("productId", "35455")
                        .queryParam("brandId", "1")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(35455))
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").value(1))

                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").value("2020-06-14-00:00:00"))

                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").value("2020-12-31-23:59:59"))

                .andExpect(MockMvcResultMatchers.jsonPath("$.finalPrice").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.finalPrice").value(35.5))

                .andExpect(MockMvcResultMatchers.jsonPath("$.isoCurrency").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.isoCurrency").value("EUR"))
        ;
    }

    @Test
    @DisplayName("request at 4:00 p.m. on the 14th for product 35455 for brand 1")
    public void test2() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders
                        .get("/price")
                        .queryParam("applicationDate", "2020-06-14-16:00:00")
                        .queryParam("productId", "35455")
                        .queryParam("brandId", "1")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(35455))
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").value(2))

                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").value("2020-06-14-15:00:00"))

                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").value("2020-06-14-18:30:00"))

                .andExpect(MockMvcResultMatchers.jsonPath("$.finalPrice").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.finalPrice").value(25.45))

                .andExpect(MockMvcResultMatchers.jsonPath("$.isoCurrency").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.isoCurrency").value("EUR"))
        ;
    }


    @Test
    @DisplayName("request at 9:00 p.m. on the 14th for product 35455 for brand 1")
    public void test3() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders
                        .get("/price")
                        .queryParam("applicationDate", "2020-06-14-21:00:00")
                        .queryParam("productId", "35455")
                        .queryParam("brandId", "1")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(35455))
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").value(1))

                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").value("2020-06-14-00:00:00"))

                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").value("2020-12-31-23:59:59"))

                .andExpect(MockMvcResultMatchers.jsonPath("$.finalPrice").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.finalPrice").value(35.5))

                .andExpect(MockMvcResultMatchers.jsonPath("$.isoCurrency").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.isoCurrency").value("EUR"))
        ;
    }

    @Test
    @DisplayName("request at 10:00 a.m. on the 15th for product 35455 for brand 1")
    public void test4() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders
                        .get("/price")
                        .queryParam("applicationDate", "2020-06-15-10:00:00")
                        .queryParam("productId", "35455")
                        .queryParam("brandId", "1")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(35455))
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").value(3))

                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").value("2020-06-15-00:00:00"))

                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").value("2020-06-15-11:00:00"))

                .andExpect(MockMvcResultMatchers.jsonPath("$.finalPrice").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.finalPrice").value(30.5))

                .andExpect(MockMvcResultMatchers.jsonPath("$.isoCurrency").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.isoCurrency").value("EUR"))
        ;
    }

    @Test
    @DisplayName("request at 9:00 p.m. on the 16th for product 35455 for brand 1")
    public void test5() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders
                        .get("/price")
                        .queryParam("applicationDate", "2020-06-16-21:00:00")
                        .queryParam("productId", "35455")
                        .queryParam("brandId", "1")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(35455))
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").value(4))

                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.startDate").value("2020-06-15-16:00:00"))

                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").value("2020-12-31-23:59:59"))

                .andExpect(MockMvcResultMatchers.jsonPath("$.finalPrice").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.finalPrice").value(38.95))

                .andExpect(MockMvcResultMatchers.jsonPath("$.isoCurrency").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.isoCurrency").value("EUR"))
        ;
    }
}
