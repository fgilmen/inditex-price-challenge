package codechallenge.app.integration.rest;

import com.indx.codechallenge.application.rest.PriceBOToGetPriceResponseConverter;
import com.indx.codechallenge.domain.price.PriceBO;
import com.indx.codechallenge.domain.price.utils.LocalDateTimeConversionUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PriceBOToGetPriceResponseConverterTest {

    public static final String LOCAL_DATE_STRING_PATTERN = "yyyy-MM-dd-HH:mm:ss";
    public static final float FINAL_PRICE = 10f;
    public static final int PRICE_LIST = 1;
    public static final long BRAND_ID = 1L;
    public static final String ISO_CURRENCY = "EUR";
    public static final String PRODUCT_ID = "7u38";
    PriceBOToGetPriceResponseConverter priceBOToGetPriceResponseConverter;
    LocalDateTimeConversionUtils localDateTimeConversionUtils;
    LocalDateTime startDate;
    LocalDateTime endDate;

    @BeforeEach
    void init () {
        localDateTimeConversionUtils = new LocalDateTimeConversionUtils(LOCAL_DATE_STRING_PATTERN);
        priceBOToGetPriceResponseConverter = new PriceBOToGetPriceResponseConverter(localDateTimeConversionUtils);
        startDate = LocalDateTime.now();
        endDate = LocalDateTime.now();
    }

    @Test
    @DisplayName("correct conversion")
    void testCorrectConversion () {
        PriceBO priceBO = PriceBO.builder()
                .finalPrice(FINAL_PRICE)
                .priceList(PRICE_LIST)
                .brandId(BRAND_ID)
                .isoCurrency(ISO_CURRENCY)
                .productId(PRODUCT_ID)
                .endDate(endDate)
                .startDate(startDate)
                .build();
        var converted = priceBOToGetPriceResponseConverter.convert(priceBO);

        assertEquals(priceBO.getPriceList(), converted.getPriceList());
        assertEquals(priceBO.getFinalPrice(), converted.getFinalPrice());
        assertEquals(priceBO.getProductId(), converted.getProductId());
        assertEquals(priceBO.getBrandId(), converted.getBrandId());
        assertEquals(priceBO.getIsoCurrency(), converted.getIsoCurrency());
        assertEquals(localDateTimeConversionUtils.convertLocalDateTimeToString(priceBO.getEndDate()), converted.getEndDate());
        assertEquals(localDateTimeConversionUtils.convertLocalDateTimeToString(priceBO.getStartDate()), converted.getStartDate());
    }

}
