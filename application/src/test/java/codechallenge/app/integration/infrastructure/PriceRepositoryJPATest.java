package codechallenge.app.integration.infrastructure;

import com.indx.codechallenge.application.Application;
import com.indx.codechallenge.domain.price.ElementsNotFoundException;
import com.indx.codechallenge.repository.PriceRepositoryJPA;
import com.indx.codechallenge.repository.entity.Price;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest(
        classes = Application.class)
public class PriceRepositoryJPATest {

    private static final String PRODUCT_ID = "35455";
    private static final long BRAND_ID = 1l;
    
    @Autowired
    PriceRepositoryJPA priceJPARepository;

    @Value("${codechallenge.pattern.stringtolocaldatetime}")
    private String dateTimeFormatter;

    @Test
    @DisplayName("request at 10:00 a.m. on the 14th for product 35455 for brand 1")
    public void test1() throws ElementsNotFoundException {
        List<Price> result = getPrices("2020-06-14-10:00:00");

        assertEquals(1, result.size());
        assertEquals(Float.valueOf(35.5f), result.get(0).getPrice());
    }

    @Test
    @DisplayName("request at 4:00 p.m. on the 14th for product 35455 for brand 1")
    public void test2() throws ElementsNotFoundException {
        List<Price> result = getPrices("2020-06-14-16:00:00");

        assertEquals(2, result.size());
        assertEquals(Float.valueOf(25.45f), result.get(0).getPrice());
    }

    @Test
    @DisplayName("request at 9:00 p.m. on the 14th for product 35455 for brand 1")
    public void test3() throws ElementsNotFoundException {
        List<Price> result = getPrices("2020-06-14-21:00:00");

        assertEquals(1, result.size());
        assertEquals(Float.valueOf(35.5f), result.get(0).getPrice());
    }

    @Test
    @DisplayName("request at 10:00 a.m. on the 15th for product 35455 for brand 1")
    public void test4() throws ElementsNotFoundException {
        List<Price> result = getPrices("2020-06-15-10:00:00");

        assertEquals(2, result.size());
        assertEquals(Float.valueOf(30.5f), result.get(0).getPrice());
    }

    @Test
    @DisplayName("request at 9:00 p.m. on the 16th for product 35455 for brand 1")
    public void test5() throws ElementsNotFoundException {
        List<Price> result = getPrices("2020-06-16-21:00:00");

        assertEquals(2, result.size());
        assertEquals(Float.valueOf(38.95f), result.get(0).getPrice());
    }

    @Test
    @DisplayName("all data is loaded")
    public void testFindAll () {
        var result = getAllPrices();
        assertEquals(4, result.size());
    }

    @Test
    @DisplayName("exception thrown in no data case")
    public void test6(){
        assertThrows(ElementsNotFoundException.class, () -> getPrices("2021-06-16-21:00:00"));
    }

    private List<Price> getPrices(String date) throws ElementsNotFoundException {
        LocalDateTime localDateTime = LocalDateTime.parse(date, DateTimeFormatter.ofPattern(dateTimeFormatter));
        List<Price> result = priceJPARepository
                .findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandIdOrderedByPriorityDesc(localDateTime, PRODUCT_ID, BRAND_ID);
        if (result.isEmpty()) {
            throw new ElementsNotFoundException();
        }

        return result;
    }

    private List<Price> getAllPrices() {
        return (List<Price>) priceJPARepository.findAll();
    }

}
