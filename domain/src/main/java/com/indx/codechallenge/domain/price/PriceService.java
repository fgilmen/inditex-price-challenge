package com.indx.codechallenge.domain.price;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PriceService {

    private PriceRepository priceRepository;

    private ConversionService conversionService;

    public PriceService(final PriceRepository priceRepository, final ConversionService conversionService) {
        this.priceRepository = priceRepository;
        this.conversionService = conversionService;
    }

    public PriceBO getPrice(final GetPriceBO getPriceBO) throws Throwable {

        List<PriceBO> prices = priceRepository.findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandIdOrderedByPriorityDesc
                (
                        getPriceBO.getLdtApplicationDate(),
                        getPriceBO.getProductId(),
                        getPriceBO.getBrandId()
                );
        //cojo el primero por que en caso de haber varios ya vienen ordenados descendentemente por prioridad

        if (prices.isEmpty()) {
            throw new ElementsNotFoundException();
        }

        return conversionService.convert(prices.get(0), PriceBO.class);
    }

}
