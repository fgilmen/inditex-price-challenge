package com.indx.codechallenge.domain.price;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;
@EqualsAndHashCode
@ToString
@Getter
@Builder
public class GetPriceBO {
    private LocalDateTime ldtApplicationDate;
    private String productId;
    private Long brandId;
}
