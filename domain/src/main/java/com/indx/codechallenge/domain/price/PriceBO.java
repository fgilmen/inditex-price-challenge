package com.indx.codechallenge.domain.price;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;
@EqualsAndHashCode
@ToString
@Getter
@Builder
public class PriceBO {
    private String productId;
    private Long brandId;
    private Integer priceList;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Float finalPrice;
    private String isoCurrency;
}
