package com.indx.codechallenge.domain.price;

import java.time.LocalDateTime;
import java.util.List;

public interface PriceRepository {
    List<PriceBO> findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandIdOrderedByPriorityDesc
            (LocalDateTime appDate,
            String productId,
            Long brandId);
}
