Price Microservice
==================
App Swagger:
http://localhost:8043/swagger-ui/index.html

/price [GET]

-   Returns the price of an item in a string on a given date

Url Call: 
---------

http://localhost:8043/price?applicationDate=2020-06-14-16:00:00&productId=35455&brandId=1


-   **applicationDate** (mandatory): Date for which the query is made.
-   **productId** (mandatory): Unique identifier of the product to query.
-   **brandId** (mandatory): Identifies a chain within Inditex.

Comments:
---------

- The external test postman can be found in the **/postman** folder.
- The original test statement can be found in the **/pdf** folder.