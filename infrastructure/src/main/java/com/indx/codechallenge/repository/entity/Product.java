package com.indx.codechallenge.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

@Entity(name = "Product")
@Table(name = "PRODUCTS")
@EqualsAndHashCode
@ToString
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class Product {

    @Id
    @Column(name = "ID", updatable = false, nullable = false, length = 20)
    private String id;

    @Column(name = "NAME", nullable = false)
    private String name;
}
