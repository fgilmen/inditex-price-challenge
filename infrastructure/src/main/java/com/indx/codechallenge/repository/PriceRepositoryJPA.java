package com.indx.codechallenge.repository;

import com.indx.codechallenge.repository.entity.Price;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface PriceRepositoryJPA extends CrudRepository<Price, Long> {

    @Query(value = "SELECT p "+
            "FROM Price p "+
            "INNER JOIN p.product pr "+
            "INNER JOIN p.brand b "+
            "WHERE b.id = :brandId "+
            "AND pr.id = :productId "+
            "AND p.startDate <= :appDate "+
            "AND p.endDate >= :appDate " +
            "ORDER BY p.priority DESC"
    )
    List<Price> findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandIdOrderedByPriorityDesc(@Param("appDate")LocalDateTime appDate,
                                                                                                                            @Param("productId") String productId,
                                                                                                                            @Param("brandId") Long brandId);
}
