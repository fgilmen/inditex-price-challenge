package com.indx.codechallenge.repository;

import com.indx.codechallenge.domain.price.PriceBO;
import com.indx.codechallenge.repository.entity.Price;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PriceToPriceBOConverter implements Converter<Price, PriceBO> {

    @Override
    public PriceBO convert(final Price price) {
        return PriceBO.builder()
                .brandId(price.getBrand().getId())
                .endDate(price.getEndDate())
                .finalPrice(price.getPrice())
                .isoCurrency(price.getIsoCurrency())
                .priceList(price.getPriceList())
                .productId(price.getProduct().getId())
                .startDate(price.getStartDate())
                .build();
    }
}
