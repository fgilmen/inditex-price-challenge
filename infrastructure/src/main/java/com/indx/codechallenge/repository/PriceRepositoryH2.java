package com.indx.codechallenge.repository;

import com.indx.codechallenge.domain.price.PriceBO;
import com.indx.codechallenge.domain.price.PriceRepository;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Component
public class PriceRepositoryH2 implements PriceRepository {

    private PriceRepositoryJPA repository;
    private ConversionService converter;

    @Override
    public List<PriceBO> findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandIdOrderedByPriorityDesc
            (LocalDateTime appDate, String productId, Long brandId) {
        return repository.findByStartDateLessThanEqualAndEndDateGreaterThanEqualAndProductIdAndBrandIdOrderedByPriorityDesc(appDate, productId, brandId).
                stream().map(m -> converter.convert(m, PriceBO.class)).collect(Collectors.toList());
    }
}
