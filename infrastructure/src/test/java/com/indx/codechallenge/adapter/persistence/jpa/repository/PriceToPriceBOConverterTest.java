package com.indx.codechallenge.adapter.persistence.jpa.repository;

import com.indx.codechallenge.repository.PriceToPriceBOConverter;
import com.indx.codechallenge.repository.entity.Brand;
import com.indx.codechallenge.repository.entity.Price;
import com.indx.codechallenge.repository.entity.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PriceToPriceBOConverterTest {

    public static final float FINAL_PRICE = 10f;
    public static final int PRICE_LIST = 1;
    public static final long BRAND_ID = 1L;
    public static final String ISO_CURRENCY = "EUR";
    public static final String PRODUCT_ID = "7u38";
    public static final String BRAND_NAME = "A BRAND";
    public static final String PRODUCT_NAME = "A PRODUCT";
    public static final int PRIORITY = 1;
    public static final long ID = 1L;
    PriceToPriceBOConverter priceToPriceBOConverter;
    LocalDateTime startDate;
    LocalDateTime endDate;

    @BeforeEach
    void init () {
        priceToPriceBOConverter = new PriceToPriceBOConverter();
        startDate = LocalDateTime.now();
        endDate = LocalDateTime.now();
    }

    @Test
    @DisplayName("correct conversion")
    void testCorrectConversion () {
        Price price = getPrice();

        var converted = priceToPriceBOConverter.convert(price);

        assertEquals(price.getPriceList(), converted.getPriceList());
        assertEquals(price.getPrice(), converted.getFinalPrice());
        assertEquals(price.getProduct().getId(), converted.getProductId());
        assertEquals(price.getBrand().getId(), converted.getBrandId());
        assertEquals(price.getIsoCurrency(), converted.getIsoCurrency());
        assertEquals(price.getEndDate(), converted.getEndDate());
        assertEquals(price.getStartDate(), converted.getStartDate());
    }

    private Price getPrice() {
        return Price.builder()
                .brand(Brand.builder()
                        .id(BRAND_ID)
                        .name(BRAND_NAME)
                        .build())
                .priceList(PRICE_LIST)
                .product(Product.builder()
                        .id(PRODUCT_ID)
                        .name(PRODUCT_NAME)
                        .build())
                .isoCurrency(ISO_CURRENCY)
                .priority(PRIORITY)
                .price(FINAL_PRICE)
                .id(ID)
                .endDate(endDate)
                .startDate(startDate)
                .build();
    }

}
